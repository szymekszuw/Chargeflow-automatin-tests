# Introduction

Automation tests for Chargeflow.io

# Getting Started

Installation process

```bash
nvm use 16
```

Once `nvm` will setup node version, dependencies can be installed with:

```
npm install
```

# Environment Documentation

This project uses the `cypress.env.json` file to store environment variables. Below is a list of variables and their meanings:

- `BASE_URL`: The main application URL.
- `API_URL`: The API URL that the application communicates with.
- `EMAIL`: The email address used for logging into the application.
- `PASSWORD`: The password used for logging in.

Please note that the `cypress.env.json` file should be kept secure and should not be stored in the code repository. Each environment variable should be provided by the user or configured in the runtime environment.

To run the project locally, create a `cypress.env.json` file in the project's root directory and define values for the above variables.

Example contents in file `cypress.env.template.json`.

# End-to-end testing

In the project directory, you can run:

### `npm run cy:open`

Opens the Cypress Test Runner.

### `npm run cy:run`

Runs Cypress tests to completion. By default, `cypress:run` will run all tests headlessly.

### `npm run cy:firefox`

Runs Cypress tests to completion on Firefox browser and will run all tests headlessly.

# Documentation

Cypress best practices - https://docs.cypress.io/guides/references/best-practices#Selecting-Elements

```
How to start - https://docs.cypress.io/guides/getting-started/opening-the-app#Quick-Configuration
```
