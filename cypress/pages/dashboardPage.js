class dashboardPage {
    elements = {
        header: () => cy.get('h3'),
        dashboardTab: () => cy.get('[href="/dashboard"]'),
        disputesTab: () => cy.get('[href="/disputes"] > #div-link').first(),
        alertsTab: () => cy.get('[href="/alerts"] > #div-link'),
        settingsTab: () => cy.get('[href="/setings"]'),
        integrationsTab: () => cy.get('[href="/integrations"]'),
        notifivationsTab: () => cy.get('[href="/notifications"]'),
    }

    checkHeader(text) {
        this.elements.header().should('contain.text', text)
    }
    visitDashboardTab() {
        this.elements.dashboardTab().click()
    }
    visitDisputesTab() {
        cy.intercept(`${Cypress.env('apiUrl')}disputes/disputes-export-request`).as('waitForDisputes')
        this.elements.disputesTab().click()
        cy.wait('@waitForDisputes');
    }
    visitAlertTab() {
        this.elements.alertsTab().click()
    }
    visitSettingsTab() {
        this.elements.settingsTab().click()
    }
    visitIntegrationsTab() {
        this.elements.integrationsTab().click()
    }
    visiTNotificationsTab() {
        this.elements.notifivationsTab().click()
    }
    checkUrl(text) {
        cy.url().should('include', text)
    }

}
export default dashboardPage;