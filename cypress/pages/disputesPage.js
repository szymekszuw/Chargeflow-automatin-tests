class disputesPage {
    elements = {
        wonFilter: () => cy.get('button').contains('Won'),
        allFiltersButton: () => cy.get('#show-filter-panel-btn'),
    }
    selectWonFlter() {
        cy.intercept(`${Cypress.env('apiUrl')}**?status_names=won*`).as('waitForFiltering')
        this.elements.wonFilter().click()
        cy.wait('@waitForFiltering').then((res) => {
            cy.log('Status code from filtering', res.response.statusCode)
            const statusCode = res.response.statusCode
            expect(statusCode).equal(200)
        });
    }

    findDispute(id) {
        cy.contains(id).click()
    }


}
export default disputesPage;