class loginPage {
    elements = {
        emailField: () => cy.get('#email'),
        passwordField: () => cy.get('#password'),
        submitButton: () => cy.get('button[type="submit"]').contains('Sign In'),

    }

    fillUserEmail(email) {
        this.elements.emailField().type(email)
    }
    fillPassword(password) {
        this.elements.passwordField().type(password)
    }
    submit() {
        cy.intercept(`${Cypress.env('apiUrl')}is/exist/user/user`).as('waitForLogin')
        this.elements.submitButton().click()
        cy.wait('@waitForLogin');
    }

}
export default loginPage;