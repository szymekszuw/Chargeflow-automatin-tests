class disputeDetails {
    elements = {
        findField: () => cy.get('#div-1-cv')
    }
    findFieldByDetails(expectedDetails, result) {
        cy.contains(expectedDetails).parent().children().last().should('have.text', result)
    }
    checkDisputeDetailFromApiCall(text) {
        cy.intercept('GET', `https://api.chargeflow.io/disputes/64f5e130d1864e6890caa236`).as('disputesDetails')
        cy.url().should('contain', '/disputes')

        cy.wait('@disputesDetails').then(res => {
            const shopPlatform = res.response.body.shopPlatform
            expect(shopPlatform).to.equal(text)
        });
    }

}
export default disputeDetails;