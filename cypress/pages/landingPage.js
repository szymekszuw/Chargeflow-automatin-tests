class landingPage {
    elements = {
        loginButton: () => cy.get('.login'),
    }
    visit() {
        cy.visit(Cypress.env('baseUrl'))
    }

    goTologinPage() {
        //.invoke('removeAttr', 'target') disables opening of the login page in a new tab
        this.elements.loginButton().invoke('removeAttr', 'target').click()
    }
    checkUrl(text) {
        cy.url().should('include', text)
    }
}
export default landingPage;