import dashboardPage from "../pages/dashboardPage"
import disputeDetails from "../pages/disputeDetails"
import disputesPage from "../pages/disputesPage"
import landingPage from "../pages/landingPage"
import loginPage from "../pages/loginPage"

describe('Automate Dispute Flow', () => {
  const LandingPg = new landingPage()
  const LoginPg = new loginPage()
  const DashboardPg = new dashboardPage()
  const DisputesPg = new disputesPage()
  const DisputeDetails = new disputeDetails()

  it('should log in as a merchant and assert dispute details', () => {
    cy.intercept('GET', `${Cypress.env('apiUrl')}disputes/64f5e130d1864e6890caa236`).as('disputesDetails')
    LandingPg.visit()
    LandingPg.goTologinPage()
    LandingPg.checkUrl('auth/sign-in')
    LoginPg.fillUserEmail(Cypress.env('email'))
    LoginPg.fillPassword(Cypress.env('password'))
    LoginPg.submit()
    DashboardPg.checkHeader('Welcome')
    DashboardPg.visitDisputesTab()
    DashboardPg.checkUrl('/disputes')
    DashboardPg.checkHeader('Disputes')
    DisputesPg.selectWonFlter()
    DisputesPg.findDispute(Cypress.env('disputeId'))
    cy.wait('@disputesDetails').then(res => {
      const shopPlatform = res.response.body.shopPlatform
      expect(shopPlatform).to.equal('shopify')
    });
    DisputeDetails.findFieldByDetails('Disputed Amount', '$59.99 USD')
    DisputeDetails.findFieldByDetails('Reason', 'NOT RECEIVED')

  })

})